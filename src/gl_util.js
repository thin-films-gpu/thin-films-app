export function initWebGL(canvas)
{
    let gl = null;

    try {
        gl = canvas.getContext('webgl2', {antialias: true});
    } catch(e) { }

    if (!gl) {
        alert('Unable to initialize WebGL. Your browser may not support it.');
        return null;
    }
    if (!gl.getExtension('EXT_color_buffer_float')) {
        console.error('no EXT_color_buffer_float!');
    }
    if (!gl.getExtension('OES_texture_float_linear')) {
        console.error('no OES_texture_float_linear!');
    }
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
    gl.clearColor(0, 0, 0, 0);
    return gl;
}

export function createShader(gl, type, source)
{
  const shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      console.error(source);
      console.error(gl.getShaderInfoLog(shader));
      gl.deleteShader(shader);
      return null;
  }
  return shader;
}

export function createProgram(gl, vertexShader, fragmentShader, attribs={})
{
  const program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  for (const [k, v] in Object.entries(attribs)) {
      gl.bindAttribLocation(program, v, k);
  }
  gl.linkProgram(program);
  if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
      console.error(gl.getProgramInfoLog(program));
      gl.deleteProgram(program);
  }
  return program;
}

export function createQuad(gl)
{
    const attribs = {'a_position': 0, 'a_texCoord': 1};

    // Positions
    const positionBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array([
            -1, -1,
            1, 1,
            1, -1,
            -1, -1,
            -1, 1,
            1, 1
        ]),
        gl.STATIC_DRAW
    );
    gl.enableVertexAttribArray(attribs['a_position']);
    gl.vertexAttribPointer(attribs['a_position'], 2, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);

    // Texture coordinates
    const texCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, texCoordBuffer);
    gl.bufferData(
        gl.ARRAY_BUFFER,
        new Float32Array([
            0, 0,
            1, 1,
            1, 0,
            0, 0,
            0, 1,
            1, 1
        ]),
        gl.STATIC_DRAW
    );
    gl.enableVertexAttribArray(attribs['a_texCoord']);
    gl.vertexAttribPointer(attribs['a_texCoord'], 2, gl.FLOAT, false, 0, 0);
    gl.bindBuffer(gl.ARRAY_BUFFER, null);
    return {attribs, buffers: [positionBuffer, texCoordBuffer]};
}

export function drawQuad(gl)
{
    gl.drawArrays(gl.TRIANGLES, 0, 6);
}

export function createTexture(gl, source, size=null, repeat=false, mipmap=false)
{
    const texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    if (source instanceof Image) {
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, source);
    } else {
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA32F, +size.width, +size.height, 0, gl.RGBA, gl.FLOAT, source);
    }
    texture.size = size;
    // Set the parameters so we can render any size image.
    if (mipmap) {
        gl.generateMipmap(gl.TEXTURE_2D);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST_MIPMAP_LINEAR);
    } else {
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, repeat ? gl.REPEAT : gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    }
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    return texture;
}

export function setTextures(gl, fbo, program, textures)
{
  let textureSlot = gl.TEXTURE0;
  let uniform = 0;
  for (const [textureName, texture] of Object.entries(textures)) {
    if (textureName === 'out') {
      if (texture === 'canvas') {
        gl.uniform1i(gl.getUniformLocation(program, 'u_flip'), true);
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
      } else {
        gl.uniform1i(gl.getUniformLocation(program, 'u_flip'), false);
        gl.bindFramebuffer(gl.FRAMEBUFFER, fbo);
        if (Array.isArray(texture)) {
          const colorAttachments = [];
          for (const subTexture of texture) {
              gl.viewport(0, 0, subTexture.size.width, subTexture.size.height);
              const colorAttachment = gl.COLOR_ATTACHMENT0 + colorAttachments.length;
              colorAttachments.push(colorAttachment);
              gl.framebufferTexture2D(gl.FRAMEBUFFER, colorAttachment, gl.TEXTURE_2D, subTexture, 0);
          }
          gl.drawBuffers(colorAttachments);
        } else {
          gl.viewport(0, 0, texture.size.width, texture.size.height);
          gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture, 0);
        }
      }
    } else {
      gl.activeTexture(textureSlot);
      gl.bindTexture(gl.TEXTURE_2D, texture);
      gl.uniform1i(gl.getUniformLocation(program, textureName), uniform);
      textureSlot++;
      uniform++;
    }
  }
}

export function download(size)
{
  //var w = gl.canvas.width;
  //var h = gl.canvas.height;
  var pixels = new Float32Array(size * size * 4);
  gl.readBuffer(gl.COLOR_ATTACHMENT0);
  gl.readPixels(0, 0, size, size, gl.RGBA, gl.FLOAT, pixels);
  var csv = 'data:text/csv;charset=utf-8';
  console.log(pixels);
  pixels.forEach(function(val,ix,A) {
    csv += ','+val;
  });
  var encodedUri = encodeURI(csv);
  window.open(encodedUri);
}