import React from 'react';
import Stepper from '@material-ui/core/Stepper/Stepper';
import MobileStepper from '@material-ui/core/MobileStepper/MobileStepper';
import Dialog from '@material-ui/core/Dialog/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import Step from '@material-ui/core/Step/Step';
import Typography from '@material-ui/core/Typography/Typography';
import GridList from '@material-ui/core/GridList/GridList';
import GridListTitle from '@material-ui/core/GridListTile/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar/GridListTileBar';
import Radio from '@material-ui/core/Radio/Radio';
import ListItem from '@material-ui/core/ListItem/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon/ListItemIcon';
import ScatterPlotIcon from '@material-ui/icons/ScatterPlot';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ListItemText from '@material-ui/core/ListItemText/ListItemText';
import Button from '@material-ui/core/Button/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails/ExpansionPanelDetails';
import List from '@material-ui/core/List/List';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction';
import StepLabel from '@material-ui/core/StepLabel/StepLabel';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';

import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';

import fluids from '../content/fluids';
import initialConditions from '../content/initial_conditions';
import backgrounds from '../content/backgrounds';
import parameters, {getParameterDefault} from '../content/parameters';

import style from './SimulationSetupDialog.sass';
import ParameterEdit from "./ParameterEdit";

class SimulationSetupDialog extends React.Component
{
    static parameterSections = [
        {
            title: 'Fluid behavior',
            subTitle: 'Diffusion, gravity, viscosity',
            parameters: [
                'periodic',
                'diffusion',
                'accelerometerGravity',
                'gravity',
                'fluidViscosity',
                {parameter: 'viscosity', enabled: state => !state.parameters.fluidViscosity},
                'mobility'
            ]
        },
        {
            title: 'Accuracy',
            subTitle: 'Simulation resolution, iteration count',
            parameters: [
                'simulationResolution',
                'dynamicIterationCount',
                {parameter: 'iterations', enabled: state => !state.parameters.dynamicIterationCount},
                'dynamicTimeStep',
                {parameter: 'logTimeStep', enabled: state => !state.parameters.dynamicTimeStep}
            ]
        },
        {
            title: 'Rendering',
            subTitle: 'How the fluid is rendered',
            parameters: ['shader', 'renderResolution', 'lightDirection', 'lightDistance', 'smoothingPasses']
        }
    ];

    static parameterPageParameterNames =
        SimulationSetupDialog.parameterSections
            .map(({parameters}) => parameters)
            .reduce((prev, curr) => [...prev, ...curr], [])
            .map(paramSpec => typeof paramSpec === 'string' ? paramSpec : paramSpec.parameter);

    static steps = [
        {
            title: 'Background',
            required: true,
            completed: state => state.parameters.background !== null,
            preview: state => backgrounds[state.parameters.background].title,
            error: state => null
        },
        {
            title: 'Fluid',
            required: true,
            completed: state => state.parameters.fluid !== null,
            preview: state => state.parameters.fluid in fluids? fluids[state.parameters.fluid].title : '',
            error: state => null
        },
        {
            title: 'Initial Condition',
            required: false,
            completed: state => state.parameters.initialCondition !== null,
            preview: state => initialConditions[state.parameters.initialCondition].title,
            error: state => null
        },
        {
            title: 'Parameters',
            required: true,
            completed: state => Object.values(state.parameters).every(value => value !== null),
            preview: state => '',
            error: state =>
                SimulationSetupDialog.parameterPageParameterNames
                    .some(paramName => state.parameters[paramName] === null)
                    ? 'Incomplete'
                    : null
        }
    ];

    static propTypes = {
        open: PropTypes.bool.isRequired,
        onContinue: PropTypes.func.isRequired,
        onBackgroundSelected: PropTypes.func,
        initialParameters:
            PropTypes.shape(
                Object.keys(parameters).reduce((obj, paramName) => ({...obj, [paramName]: PropTypes.any}), {})
            ).isRequired,
        width: PropTypes.string.isRequired
    };

    constructor(props)
    {
        super(props);
        this.state = {
            step: 0,
            expandedSectionIndex: 0,
            parameters:
                Object.keys(parameters).reduce(
                    (obj, paramName) => ({...obj, [paramName]: this.props.initialParameters[paramName]}),
                    {}
                ),
            normalFlow: true
        };
    }

    selectBackground(backgroundId)
    {
        this.setState({parameters: {...this.state.parameters, background: backgroundId}});
        if (this.props.onBackgroundSelected) {
            this.props.onBackgroundSelected(backgroundId);
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if (!prevProps.open && this.props.open) {
            this.state.parameters = Object.keys(parameters).reduce(
                (obj, paramName) => ({...obj, [paramName]: this.props.initialParameters[paramName]}),
                {}
            );
        }

        if (this.state.normalFlow) {
            for (const [stepId, step] of Object.entries(SimulationSetupDialog.steps)) {
                if (!step.completed(prevState)
                    && step.completed(this.state)
                    && +stepId !== (SimulationSetupDialog.steps.length - 1)) {
                    this.setState({step: +stepId + 1});
                }
            }
        }
    }

    resetParameterDefaults()
    {
        this.setState({parameters: {
            ...this.state.parameters,
            ...SimulationSetupDialog.parameterPageParameterNames.reduce(
                (obj, paramName) => ({...obj, [paramName]: getParameterDefault(parameters[paramName])}),
                {}
            )
        }});
    }

    renderStep(stepId)
    {
        switch (stepId) {
            case 0:
                return <GridList key={stepId} className={style.backgroundSelector}>
                    {
                        Object.entries(backgrounds).map(
                            ([bgId, bg]) => (
                                <GridListTitle
                                        key={bgId}
                                        className={style.item}
                                        onClick={() => this.selectBackground(bgId)}
                                    >
                                    <img src={bg.diffuse} />
                                    <GridListTileBar
                                        title={bg.title}
                                        actionIcon={
                                            <Radio
                                                checked={this.state.parameters.background === bgId}
                                                value={bgId}
                                                color='secondary'
                                                onChange={
                                                    (event, checked) => {
                                                         if (checked) {
                                                             this.selectBackground(bgId);
                                                         }
                                                    }
                                                }
                                            />
                                        }
                                    />
                                </GridListTitle>
                            )
                        )
                    }
                </GridList>;
            case 1:
                return <div key={stepId} className={style.fluidSelector}>
                    {
                        Object.entries(fluids).map(
                            ([fluidId, {color: [r, g, b], title}]) => <ListItem key={fluidId} className={style.item}>
                                <ListItemIcon style={{color: 'rgba(' + (r * 255) + ', ' + (g * 255) + ', '
                                                                     + (b * 255) + ', 1)'}}>
                                    <ScatterPlotIcon />
                                </ListItemIcon>
                                <ListItemText primary={title} />
                                <Radio checked={this.state.parameters.fluid === fluidId}
                                       value={fluidId}
                                       onChange={
                                           (event, checked) => {
                                                if (checked) {
                                                    this.setState({
                                                        parameters: {...this.state.parameters, fluid: fluidId}
                                                    });
                                                }
                                           }
                                       }
                                />
                            </ListItem>
                        )
                    }
                </div>;
            case 2:
                return <GridList
                        key={stepId}
                        className={style.initialConditionSelector}
                        cols={isWidthUp('sm', this.props.width) ? 3 : 2}
                    >
                    {
                        Object.entries(initialConditions).map(
                            ([conditionId, condition]) => (
                                <GridListTitle
                                        key={conditionId}
                                        className={style.item}
                                        onClick={
                                            () => this.setState({
                                                parameters: {...this.state.parameters, initialCondition: conditionId}
                                            })
                                        }
                                    >
                                    <img src={condition.path} />
                                    <GridListTileBar
                                        title={condition.title}
                                        actionIcon={
                                            <Radio
                                                checked={this.state.parameters.initialCondition === conditionId}
                                                value={conditionId}
                                                color='secondary'
                                                onChange={
                                                    (event, checked) => {
                                                         if (checked) {
                                                            this.setState({
                                                                parameters: {
                                                                    ...this.state.parameters,
                                                                    initialCondition: conditionId
                                                                }
                                                            });
                                                         }
                                                    }
                                                }
                                            />
                                        }
                                    />
                                </GridListTitle>
                            )
                        )
                    }
                </GridList>;
            case 3:
                return <div key={stepId} className={style.parametersTab}>
                    <div className={style.header}>
                        <Typography>
                            Here you can tweak advanced parameters of the simulation (you can also tweak these later through the tweak bar)
                        </Typography>
                        <Button variant='flat' onClick={() => this.resetParameterDefaults()}>Reset defaults</Button>
                    </div>
                    {SimulationSetupDialog.parameterSections.map((section, sectionIndex) => (
                        <ExpansionPanel
                                key={sectionIndex}
                                expanded={this.state.expandedSectionIndex === sectionIndex}
                                onChange={
                                    (event, expanded) => this.setState({
                                        expandedSectionIndex: expanded ? sectionIndex : null
                                    })
                                }
                            >
                            <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon />}
                                    classes={{content: style.sectionSummary}}
                                >
                                <Typography className={style.sectionHeading}>
                                    {section.title}
                                </Typography>
                                <Typography className={style.sectionSecondaryHeading} variant='caption'>
                                    {section.subTitle}
                                </Typography>
                            </ExpansionPanelSummary>
                            <ExpansionPanelDetails className={style.parameterSection}>
                                <List style={{width: '100%'}}>
                                    {
                                        section.parameters
                                            .map(
                                                paramSpec => typeof paramSpec === 'string'
                                                    ? {paramName: paramSpec, parameter: parameters[paramSpec]}
                                                    : {
                                                        ...paramSpec,
                                                        paramName: paramSpec.parameter,
                                                        parameter: parameters[paramSpec.parameter]
                                                    }
                                            )
                                            .map(({paramName, parameter, enabled=() => true}) => (
                                                <ListItem
                                                        key={paramName}
                                                        disabled={!enabled(this.state)}
                                                        classes={{
                                                            root: style.parameterHeader,
                                                            container: style.parameterRow
                                                        }}
                                                    >
                                                    <ListItemIcon>{parameter.icon}</ListItemIcon>
                                                    <ListItemText className={style.parameterName}>
                                                        {parameter.title}
                                                    </ListItemText>
                                                    <ListItemSecondaryAction className={style.parameterAction}>
                                                        <ParameterEdit
                                                            parameter={parameter}
                                                            value={this.state.parameters[paramName]}
                                                            disabled={!enabled(this.state)}
                                                            onChange={
                                                                value => this.setState({
                                                                    parameters: {
                                                                        ...this.state.parameters,
                                                                        [paramName]: value
                                                                    }
                                                                })
                                                            }/>
                                                    </ListItemSecondaryAction>
                                                </ListItem>
                                            ))
                                    }
                                </List>
                            </ExpansionPanelDetails>
                        </ExpansionPanel>
                    ))}
                </div>;
        }
    }

    render()
    {
        const isDone = Object.values(SimulationSetupDialog.steps).every(step => step.completed(this.state));
        return <Dialog open={this.props.open}>
            <DialogTitle>Set up a scene</DialogTitle>
            {
                isWidthUp('sm', this.props.width)
                    ? <Stepper className={style.stepper} alternativeLabel nonLinear activeStep={this.state.step}>
                        {
                            Object.entries(SimulationSetupDialog.steps)
                                .map(
                                    ([stepId, step]) =>
                                        <Step key={stepId} completed={step.completed(this.state)} >
                                            <StepLabel
                                                    className={style.step}
                                                    error={step.error(this.state)}
                                                    optional={
                                                        step.completed(this.state)
                                                            ? <Typography
                                                                    variant='caption'
                                                                    className={style.stepperStepCaption}
                                                                >
                                                                {step.preview(this.state)}
                                                            </Typography>
                                                            : null
                                                    }
                                                    onClick={() => this.setState({step: +stepId, normalFlow: false})}
                                                    last={stepId === SimulationSetupDialog.steps.length - 1}
                                                >
                                                {step.title}
                                            </StepLabel>
                                        </Step>
                                )
                        }
                    </Stepper>
                    : <div className={style.mobileStepperContainer}>
                        <MobileStepper
                            steps={SimulationSetupDialog.steps.length}
                            position='static'
                            activeStep={this.state.step}
                            className={style.mobileStepper}
                            nextButton={
                                <Button
                                        size='small'
                                        onClick={() => this.setState({step: this.state.step + 1})}
                                        disabled={this.state.step === SimulationSetupDialog.steps.length - 1}
                                    >
                                    Next
                                    <KeyboardArrowRight />
                                </Button>
                            }
                            backButton={
                                <Button
                                        size='small'
                                        onClick={() => this.setState({step: this.state.step - 1})}
                                        disabled={this.state.step === 0}
                                    >
                                    <KeyboardArrowLeft />
                                    Back
                                </Button>
                            }
                        />
                        <Typography className={style.mobileStepperCaption}>
                            {SimulationSetupDialog.steps[this.state.step].title}
                        </Typography>
                    </div>
            }
            <SwipeableViews
                    animateHeight
                    animateTransitions
                    index={this.state.step}
                    onChangeIndex={index => this.setState({step: index})}
                    className={style.pages}
                >
                {Object.keys(SimulationSetupDialog.steps).map(stepId => this.renderStep(+stepId))}
            </SwipeableViews>
            <Button
                    disabled={!isDone}
                    variant='raised'
                    color='primary'
                    onClick={event => this.props.onContinue(this.state.parameters)}
                >
                Start
            </Button>
        </Dialog>;
    }
}

export default withWidth()(SimulationSetupDialog);