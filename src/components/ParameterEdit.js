import React from 'react';
import Select from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import Switch from '@material-ui/core/Switch/Switch';

import RangedNumericInput from './RangedNumericInput';

export default function ParameterEdit(props)
{
    const {parameter, value, disabled=false, onChange} = props;
    switch (parameter.kind) {
        case 'bool':
            return <Switch checked={value} onChange={(event, value) => onChange(value)} />;
        case 'select':
            return <Select value={value} onChange={event => onChange(event.target.value)}>{
                parameter.options.map(({value, title}) => <MenuItem key={value} value={value}>{title}</MenuItem>)
            }</Select>;

        case 'number':
            return <RangedNumericInput
                min={parameter.range.reasonable.from}
                max={parameter.range.reasonable.to}
                strictLowerBound={parameter.range.valid.from}
                strictUpperBound={parameter.range.valid.to}
                closestAcceptable={parameter.range.closestAcceptable}
                disabled={disabled}
                value={value}
                onChange={value => onChange(+value)}
            />;
        default:
            return null;
    }
}