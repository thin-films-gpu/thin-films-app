import React from 'react';
import PropTypes from 'prop-types';
import equals from 'fast-deep-equal';
import classNames from 'classnames';

import {createProgram, createQuad, createShader, createTexture, drawQuad, initWebGL, setTextures} from '../gl_util';
import causticsShaderSource from '../shaders/caustics.frag';
import dewetShaderSource from '../shaders/dewet.frag';
import gradientShaderSource from '../shaders/gradient.frag';
import passShaderSource from '../shaders/pass.frag';
import heightMapShaderSource from '../shaders/heightmap.frag';
import quadShaderSource from '../shaders/quad.vert';
import refractionShaderSource from '../shaders/refraction.frag';
import sprayShaderSource from '../shaders/spray.frag';
import thinFilmsShaderSource from '../shaders/thin_films.frag';
import blackShaderSource from '../shaders/black.frag';
import normalShaderSource from '../shaders/normal.frag';
import blurShaderSource from '../shaders/blur.frag';

import style from './ThinFilmsSimulation.sass'
import {image} from '../util';
import backgrounds from "../content/backgrounds";
import parameters from "../content/parameters";

export default class ThinFilmsSimulation extends React.Component
{
    static shaders = {
        kHeightmap: 'heightmap',
        kGradient: 'gradient',
        kNormal: 'normal',
        kRefraction: 'refraction',
        kRefractionAndCaustics: 'caustics'
    };

    static renderResolution = {
        kNormal: 'normal',
        kStretch: 'stretch',
        kFull: 'full'
    };

    static propTypes = {
        className: PropTypes.string,
        simulationResolution: PropTypes.number.isRequired,
        screenWidth: PropTypes.number.isRequired,
        screenHeight: PropTypes.number.isRequired,
        renderResolution: PropTypes.oneOf(Object.values(ThinFilmsSimulation.renderResolution)).isRequired,
        background: PropTypes.shape({
            diffuse: PropTypes.string.isRequired,
            bump: PropTypes.string,
            bumpDepth: PropTypes.number
        }).isRequired,
        fluid: PropTypes.shape({
            refractiveIndex: PropTypes.number.isRequired,
            color: PropTypes.arrayOf(PropTypes.number).isRequired,
            clarity: PropTypes.shape({
                clearDepth: PropTypes.number.isRequired,
                opaqueDepth: PropTypes.number.isRequired
            }),
            viscosity: PropTypes.number.isRequired
        }).isRequired,
        initialConditionPath: PropTypes.string.isRequired,
        running: PropTypes.bool.isRequired,
        paused: PropTypes.bool.isRequired,
        diffusion: PropTypes.number.isRequired,
        viscosity: PropTypes.number,
        mobility: PropTypes.oneOf([0, 1]).isRequired,
        gravity: PropTypes.shape({
            strength: PropTypes.number.isRequired
        }).isRequired,
        iterations: PropTypes.number.isRequired,
        dynamicTimeStep: PropTypes.bool.isRequired,
        logTimeStep: PropTypes.number,
        isDewetting: PropTypes.bool.isRequired,
        brushRadius: PropTypes.number.isRequired,
        shader: PropTypes.oneOf(Object.values(ThinFilmsSimulation.shaders)).isRequired,
        periodic: PropTypes.bool.isRequired,
        reportFrameRate: PropTypes.func.isRequired,
        lightLocation: PropTypes.shape({
            x: PropTypes.number.isRequired,
            y: PropTypes.number.isRequired,
            z: PropTypes.number.isRequired
        }).isRequired,
        smoothingPasses: PropTypes.number.isRequired,
        sceneResetCounter: PropTypes.number,
        sceneSnapCounter: PropTypes.number,
        onSnap: PropTypes.func
    };

    constructor(props)
    {
        super(props);
        this.canvas = React.createRef();
        this.canvasContainer = React.createRef();
        this.state = {};
        this.animationFrameHandle = null;
        this.gl = null;
        this.fbo = null;
        this.fbo2 = null;
        this.shaders = {};
        this.programs = {};
        this.textures = {};
        this.buffers = [];
        this.isClicking = false;
        this.pendingClicks = [];
        this.lastClickLocation = null;
        this.estimatedFrameTime = .02;
        this.shouldSnap = false;
        this.totalTime = 0;
        this.nextFrame = 0;
        this.ready = false;
        this.angle = 0;
        this.steepness = 1;
    }

    screenHeightToWidthRatio()
    {
        return this.props.screenHeight / this.props.screenWidth;
    }


    renderResolution()
    {
        return {
            [ThinFilmsSimulation.renderResolution.kNormal]: this.props.simulationResolution,
            [ThinFilmsSimulation.renderResolution.kStretch]: this.props.simulationResolution,
            [ThinFilmsSimulation.renderResolution.kFull]: Math.max(this.props.screenWidth, this.props.screenHeight)
        }[this.props.renderResolution];
    }

    componentDidMount()
    {
        this.start();
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if (this.gl !== null) {
            if (ThinFilmsSimulation.arePropsCompatible(prevProps, this.props)) {
                if (prevProps.sceneSnapCounter < this.props.sceneSnapCounter) {
                    this.shouldSnap = true;
                }
                if (!prevProps.running && this.props.running && this.animationFrameHandle === null && this.ready) {
                    this.animationFrameHandle = requestAnimationFrame(() => this.draw());
                }
                if (prevProps.running && !this.props.running) {
                    cancelAnimationFrame(this.animationFrameHandle);
                    this.animationFrameHandle = null;
                }
                return;
            }
            this.stop();
        }

        this.start();
    }

    static arePropsCompatible(prevProps, newProps)
    {
        return (
            newProps.simulationResolution === prevProps.simulationResolution
            && newProps.screenWidth === prevProps.screenWidth
            && newProps.screenHeight === prevProps.screenHeight
            && equals(newProps.background, prevProps.background)
            && equals(newProps.initialConditionPath, prevProps.initialConditionPath)
            && newProps.sceneResetCounter === prevProps.sceneResetCounter
            && !(newProps.periodic && !prevProps.periodic)
        )
    }

    loadTexture(name, path, callback)
    {
        const texture = new Image();
        const prevGL = this.gl;
        texture.onload = () => {
            if (this.gl !== prevGL) {
                // We were cancelled.
                return;
            }
            const prevTexture = this.textures[name];
            this.textures[name] = createTexture(this.gl, texture);
            if (prevTexture) {
                this.gl.deleteTexture(prevTexture);
            }
            if (callback) {
                callback();
            }
        };
        texture.src = path;
    }

    start()
    {
        this.gl = initWebGL(this.canvas.current);
        if (!this.gl) {
            return;
        }
        this.totalTime = 0;
        this.nextFrame = 0;
        this.ready = false;
        const {attribs, buffers} = createQuad(this.gl);
        this.buffers.push.apply(this.buffers, buffers);

        // Set up shaders & program
        this.shaders.vertex = createShader(this.gl, this.gl.VERTEX_SHADER, quadShaderSource);
        this.shaders.thinFilms = createShader(this.gl, this.gl.FRAGMENT_SHADER, thinFilmsShaderSource);
        this.shaders.normal = createShader(this.gl, this.gl.FRAGMENT_SHADER, normalShaderSource);
        this.shaders.blur = createShader(this.gl, this.gl.FRAGMENT_SHADER, blurShaderSource);
        this.shaders.pass = createShader(this.gl, this.gl.FRAGMENT_SHADER, passShaderSource);
        this.shaders.heightmap = createShader(this.gl, this.gl.FRAGMENT_SHADER, heightMapShaderSource);
        this.shaders.gradient = createShader(this.gl, this.gl.FRAGMENT_SHADER, gradientShaderSource);
        this.shaders.spray = createShader(this.gl, this.gl.FRAGMENT_SHADER, sprayShaderSource);
        this.shaders.dewet = createShader(this.gl, this.gl.FRAGMENT_SHADER, dewetShaderSource);
        this.shaders.refraction = createShader(this.gl, this.gl.FRAGMENT_SHADER, refractionShaderSource);
        this.shaders.caustics = createShader(this.gl, this.gl.FRAGMENT_SHADER, causticsShaderSource);
        this.shaders.black = createShader(this.gl, this.gl.FRAGMENT_SHADER, blackShaderSource);

        this.programs.thinFilms = createProgram(this.gl, this.shaders.vertex, this.shaders.thinFilms, attribs);
        this.programs.normal = createProgram(this.gl, this.shaders.vertex, this.shaders.normal, attribs);
        this.programs.blur = createProgram(this.gl, this.shaders.vertex, this.shaders.blur, attribs);
        this.programs.pass = createProgram(this.gl, this.shaders.vertex, this.shaders.pass, attribs);
        this.programs.heightmap = createProgram(this.gl, this.shaders.vertex, this.shaders.heightmap, attribs);
        this.programs.gradient = createProgram(this.gl, this.shaders.vertex, this.shaders.gradient, attribs);
        this.programs.spray = createProgram(this.gl, this.shaders.vertex, this.shaders.spray, attribs);
        this.programs.dewet = createProgram(this.gl, this.shaders.vertex, this.shaders.dewet, attribs);
        this.programs.refraction = createProgram(this.gl, this.shaders.vertex, this.shaders.refraction, attribs);
        this.programs.caustics = createProgram(this.gl, this.shaders.vertex, this.shaders.caustics, attribs);
        this.programs.black = createProgram(this.gl, this.shaders.vertex, this.shaders.black, attribs);

        // Set up textures
        const canvasSize = {width: this.canvas.current.offsetWidth, height: this.canvas.current.offsetHeight};

        const simulationSize =
            this.props.screenHeight > this.props.screenWidth
                ? {
                    width: this.props.simulationResolution / this.screenHeightToWidthRatio(),
                    height: this.props.simulationResolution
                }
                : {
                    width: this.props.simulationResolution,
                    height: this.props.simulationResolution * this.screenHeightToWidthRatio()
                };
        this.textures.work1 = createTexture(this.gl, this.canvas.current, simulationSize, /*repeat=*/true);
        this.textures.work2 = createTexture(this.gl, this.canvas.current, simulationSize, /*repeat=*/true);
        this.textures.fluid = createTexture(this.gl, this.canvas.current, simulationSize, /*repeat=*/true);
        this.textures.caustics1 = createTexture(this.gl, this.canvas.current, canvasSize);
        this.textures.caustics2 = createTexture(this.gl, this.canvas.current, canvasSize);
        this.textures.caustics3 = createTexture(this.gl, this.canvas.current, canvasSize);
        this.textures.caustics4 = createTexture(this.gl, this.canvas.current, canvasSize);
        this.textures.normals = createTexture(this.gl, this.canvas.current, canvasSize);
        this.textures.normalsWork = createTexture(this.gl, this.canvas.current, canvasSize);

        // Set up the output framebuffers
        this.fbo = this.gl.createFramebuffer();
        this.fbo2 = this.gl.createFramebuffer();

        const texturesToLoad = {
            ground: this.props.background.diffuse,
            bump: backgrounds.bricks.bump || image('empty.png'),
            viridis: image('viridis.png'),
            U0: this.props.initialConditionPath
        };
        let loadedTextures = [];
        for (const [textureName, texturePath] of Object.entries(texturesToLoad)) {
            this.loadTexture(
                textureName,
                texturePath,
                () => {
                    loadedTextures.push(textureName);
                    if (loadedTextures.length === Object.keys(texturesToLoad).length) {
                        // All textures have finished loading - start animating.
                        this.ready = true;
                        if (this.props.running) {
                            this.animationFrameHandle = requestAnimationFrame(() => this.draw());
                        }
                    }
                }
            );
        }
    }

    draw()
    {
        this.gl.clear(this.gl.COLOR_BUFFER_BIT);

        let currentProgram = null;
        if (this.nextFrame === 0) {
            currentProgram = this.programs.pass;
            this.gl.useProgram(currentProgram);
            setTextures(this.gl, this.fbo, currentProgram, {'u': this.textures.U0, 'out': this.textures.work1});
            drawQuad(this.gl);
        }

        currentProgram = this.programs.thinFilms;
        this.gl.useProgram(currentProgram);
        const parLoc = this.gl.getUniformLocation(currentProgram, 'parity');
        const dijLoc = this.gl.getUniformLocation(currentProgram, 'Dij');
        this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'angle'), this.angle);
        this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'tilt'), this.steepness);
        this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'eta'), this.props.diffusion);
        this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'G'), this.props.gravity.strength);
        this.gl.uniform1i(this.gl.getUniformLocation(currentProgram, 'mobility'), this.props.mobility);
        this.gl.uniform1f(
            this.gl.getUniformLocation(currentProgram, 'bumpDepth'),
            this.props.background.bumpDepth || 0
        );
        this.gl.uniform1f(
            this.gl.getUniformLocation(currentProgram, 'tau'),
            this.props.paused
                ? 0
                : this.props.dynamicTimeStep
                    ? Math.min(
                        10 * (this.estimatedFrameTime / this.props.iterations),
                        Math.pow(10, parameters.logTimeStep.range.reasonable.to)
                    )
                    : Math.pow(10, this.props.logTimeStep)
        );
        this.gl.uniform1f(
            this.gl.getUniformLocation(currentProgram, 'epsilon'),
            this.props.viscosity || this.props.fluid.viscosity
        );
        this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'periodic'), this.props.periodic);

        const startTime = new Date().getTime();
        for (let iter = 0; iter < this.props.iterations; iter++) {
            this.gl.uniform2i(dijLoc, 1, 0); // uniform ivec2 Dij;
            this.gl.uniform1i(parLoc, 0); // uniform int parity;
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work1, 'out': this.textures.work2, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
            this.gl.uniform1i(parLoc, 1);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work2, 'out': this.textures.work1, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
            this.gl.uniform1i(parLoc, 2);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work1, 'out': this.textures.work2, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
            this.gl.uniform1i(parLoc, 3);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work2, 'out': this.textures.work1, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);

            this.gl.uniform2i(dijLoc, 0, 1); // uniform ivec2 Dij;
            this.gl.uniform1i(parLoc, 0); // uniform int parity;
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work1, 'out': this.textures.work2, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
            this.gl.uniform1i(parLoc, 1);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work2, 'out': this.textures.work1, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
            this.gl.uniform1i(parLoc, 2);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work1, 'out': this.textures.work2, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
            this.gl.uniform1i(parLoc, 3);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work2, 'out': this.textures.work1, 'bump': this.textures.bump}
            );
            drawQuad(this.gl);
        }

        if (this.pendingClicks.length > 0 || this.isClicking) {
            const click = this.pendingClicks.length > 0 ? this.pendingClicks.pop() : this.lastClickLocation;

            currentProgram = this.programs[this.props.isDewetting ? 'dewet' : 'spray'];
            this.gl.useProgram(currentProgram);
            this.gl.uniform2f(this.gl.getUniformLocation(currentProgram, 'v_click'), click.x, click.y);
            this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'radius'), this.props.brushRadius / 10);
            this.gl.uniform1f(
                this.gl.getUniformLocation(currentProgram, 'heightToWidthRatio'),
                this.screenHeightToWidthRatio()
            );
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work1, 'out': this.textures.work2}
            );
            drawQuad(this.gl);

            currentProgram = this.programs.pass;
            this.gl.useProgram(currentProgram);
            setTextures(this.gl, this.fbo, currentProgram, {'u': this.textures.work2, 'out': this.textures.work1});
            drawQuad(this.gl);
        }

        currentProgram = this.programs.pass;
        this.gl.useProgram(currentProgram);
        setTextures(
            this.gl,
            this.fbo,
            currentProgram,
            {'u': this.textures.work1, 'out': this.textures.fluid}
        );
        drawQuad(this.gl);

        currentProgram = this.programs.blur;
        this.gl.useProgram(currentProgram);
        const dirLoc = this.gl.getUniformLocation(currentProgram, 'dir');
        for (let i = 0; i < this.props.smoothingPasses; ++i) {
            this.gl.uniform2f(dirLoc, 0, 1);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.fluid, 'out': this.textures.work2}
            );
            drawQuad(this.gl);
            this.gl.uniform2f(dirLoc, 1, 0);
            setTextures(
                this.gl,
                this.fbo,
                currentProgram,
                {'u': this.textures.work2, 'out': this.textures.fluid}
            );
            drawQuad(this.gl);
        }

        currentProgram = this.programs.normal;
        this.gl.useProgram(currentProgram);
        setTextures(this.gl, this.fbo, currentProgram, {'u': this.textures.fluid, 'out': this.textures.normals});
        drawQuad(this.gl);

        const causticsTextures = [
            this.textures.caustics1,
            this.textures.caustics2,
            this.textures.caustics3,
            this.textures.caustics4
        ];
        if (this.props.shader === ThinFilmsSimulation.shaders.kRefractionAndCaustics) {
            currentProgram = this.programs.caustics;
            this.gl.useProgram(currentProgram);
            const {x, y, z} = this.props.lightLocation;
            this.gl.uniform3f(this.gl.getUniformLocation(currentProgram, 'L'), -x, -y, -z);
            setTextures(
                this.gl,
                this.fbo2,
                currentProgram,
                {'u': this.textures.fluid, 'normals': this.textures.normals, 'out': causticsTextures}
            );
            drawQuad(this.gl);
        } else {
            currentProgram = this.programs.black;
            this.gl.useProgram(currentProgram);
            for (const texture of causticsTextures) {
                setTextures(this.gl, this.fbo, currentProgram, {'out': texture});
                drawQuad(this.gl);
            }
        }
        if ([ThinFilmsSimulation.shaders.kRefractionAndCaustics,
             ThinFilmsSimulation.shaders.kRefraction].includes(this.props.shader)) {
            currentProgram = this.programs.refraction;
            this.gl.useProgram(currentProgram);
            this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'fluidRefractiveIndex'), this.props.fluid.refractiveIndex);
            const [r, g, b] = this.props.fluid.color;
            this.gl.uniform3f(this.gl.getUniformLocation(currentProgram, 'fluidColor'), r, g, b);
            const {clearDepth, opaqueDepth} = this.props.fluid.clarity;
            this.gl.uniform2f(this.gl.getUniformLocation(currentProgram, 'fluidClarity'), clearDepth, opaqueDepth);
            setTextures(this.gl, this.fbo, currentProgram, {
                'u': this.textures.fluid,
                'normals': this.textures.normals,
                'caustics1': this.textures.caustics1,
                'caustics2': this.textures.caustics2,
                'caustics3': this.textures.caustics3,
                'caustics4': this.textures.caustics4,
                'groundTexture': this.textures.ground,
                'out': 'canvas'
            });
            drawQuad(this.gl);
        } else if (this.props.shader === ThinFilmsSimulation.shaders.kGradient) {
            currentProgram = this.programs.gradient;
            this.gl.useProgram(currentProgram);
            setTextures(this.gl, this.fbo, currentProgram, {
                'gradient': this.textures.viridis,
                'u': this.textures.fluid,
                'out': 'canvas'
            });
            drawQuad(this.gl);
        } else if (this.props.shader === ThinFilmsSimulation.shaders.kNormal) {
            currentProgram = this.programs.pass;
            this.gl.useProgram(currentProgram);
            setTextures(this.gl, this.fbo, currentProgram, {'u': this.textures.normals, 'out': 'canvas'});
            drawQuad(this.gl);
        } else {
            currentProgram = this.programs.heightmap;
            this.gl.useProgram(currentProgram);
            this.gl.uniform1f(this.gl.getUniformLocation(currentProgram, 'threshold'), 2.);
            setTextures(this.gl, this.fbo, currentProgram, {'u': this.textures.fluid, 'out': 'canvas'});
            drawQuad(this.gl);
        }

        if (this.shouldSnap) {
            this.shouldSnap = false;
            this.snap();
        }

        this.nextFrame++;
        this.animationFrameHandle = requestAnimationFrame(
            () => {
                const endTime = new Date().getTime();
                this.estimatedFrameTime = (endTime - startTime) / 1000.;
                if (!this.props.paused) {
                    this.totalTime += this.estimatedFrameTime;
                }
                if (this.nextFrame % 10 === 0) {
                    if (this.props.reportFrameRate) {
                        this.props.reportFrameRate(1 / this.estimatedFrameTime);
                    }
                }/*
                if (frameTime > (1 / 30.)) {
                    if (document.getElementById('iter').value > 3) {
                        document.getElementById('iter').value = document.getElementById('iter').value - 1;
                    } /*else if (imageLevel != 1) {
                        imageLevel--;
                        start();
                        return;
                    }*
                } else if (frameTime < (1 / 60.)) {
                    /*if (imageLevel != 3) {
                        imageLevel++;
                        imageLevel++;
                        start();
                        return;
                    }else *if (document.getElementById('iter').value < 100) {
                       //document.getElementById('iter').value = document.getElementById('iter').value * 1 + 1;
                    }
                }*/
                this.draw();
            }
        );
    }

    stop()
    {
        if (this.animationFrameHandle !== null) {
            cancelAnimationFrame(this.animationFrameHandle);
        }
        this.animationFrameHandle = null;
        this.gl.deleteFramebuffer(this.fbo);
        this.gl.deleteFramebuffer(this.fbo2);
        for (const texture of Object.values(this.textures)) {
            this.gl.deleteTexture(texture);
        }
        this.textures = {};
        for (const program of Object.values(this.programs)) {
            this.gl.deleteProgram(program);
        }
        this.programs = {};
        for (const shader of Object.values(this.shaders)) {
            this.gl.deleteShader(shader);
        }
        this.shaders = {};
        for (const buffer of this.buffers) {
            this.gl.deleteBuffer(buffer);
        }
        this.buffers = [];
        this.gl = null;
        this.pendingClicks = [];
        this.lastClickLocation = null;
    }

    snap()
    {
        this.props.onSnap(this.canvas.current.toDataURL('image/png').replace('image/png', 'image/octet-stream'));
    }

    getRelativeInputLocation(x, y)
    {
        if (this.props.renderResolution === ThinFilmsSimulation.renderResolution.kNormal) {
            const canvas = this.canvas.current;
            return {
                x: (x - canvas.offsetLeft) / canvas.offsetWidth,
                y: (y - canvas.offsetTop) / canvas.offsetHeight
            };
        }
        const container = this.canvasContainer.current;
        return {x: x / container.offsetWidth, y: y / container.offsetHeight};
    }

    onCanvasInputStart(event, x, y)
    {
        this.lastClickLocation = this.getRelativeInputLocation(x, y);
        this.isClicking = true;
        this.pendingClicks.unshift(this.lastClickLocation);
        event.preventDefault();
        event.stopPropagation();
    }

    onCanvasInputMove(event, x, y)
    {
        if (!this.isClicking) {
            return;
        }

        this.lastClickLocation = this.getRelativeInputLocation(x, y);
        this.pendingClicks.unshift(this.lastClickLocation);
    }

    onCanvasInputEnd(event)
    {
        this.isClicking = false;
        event.preventDefault();
        event.stopPropagation();
    }

    render()
    {
        const canvasSize =
            this.props.screenHeight > this.props.screenWidth
                ? {
                    width: this.renderResolution() / this.screenHeightToWidthRatio(),
                    height: this.renderResolution()
                }
                : {
                    width: this.renderResolution(),
                    height: this.renderResolution() * this.screenHeightToWidthRatio()
                };
        canvasSize.width = Math.min(canvasSize.width, this.props.screenWidth);
        canvasSize.height = Math.min(canvasSize.height, this.props.screenHeight);
        return (
            <div
                    className={
                        classNames(
                            style.canvasContainer,
                            this.props.className,
                            {
                                [style.dewetting]: this.props.isDewetting,
                                [style.stopped]: !this.props.running
                            }
                        )
                    }
                    onMouseDown={
                        event => this.onCanvasInputStart(
                            event,
                            event.pageX - event.target.offsetLeft,
                            event.pageY - event.target.offsetTop
                        )
                    }
                    onMouseMove={
                        event => this.onCanvasInputMove(
                            event,
                            event.pageX - event.target.offsetLeft,
                            event.pageY - event.target.offsetTop
                        )
                    }
                    onMouseUp={event => this.onCanvasInputEnd(event)}
                    onTouchStart={
                        event => this.onCanvasInputStart(
                            event,
                            event.touches[0].pageX - event.target.offsetLeft,
                            event.touches[0].pageY - event.target.offsetTop,
                        )
                    }
                    onTouchMove={
                        event => this.onCanvasInputMove(
                            event,
                            event.touches[0].pageX - event.target.offsetLeft,
                            event.touches[0].pageY - event.target.offsetTop,
                        )
                    }
                    onTouchEnd={event => this.onCanvasInputEnd(event)}
                    ref={this.canvasContainer}
                >
                <canvas
                    width={canvasSize.width}
                    height={canvasSize.height}
                    style={
                        this.props.renderResolution === ThinFilmsSimulation.renderResolution.kStretch
                            ? {
                                transform:
                                    `scale(`
                                    + `  ${this.props.screenWidth / canvasSize.width},`
                                    + `  ${this.props.screenHeight / canvasSize.height}`
                                    + `)`
                            }
                            : null
                    }
                    ref={this.canvas}
                />
            </div>
        );
    }

}