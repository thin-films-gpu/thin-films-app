import React from 'react';
import PropTypes from 'prop-types';
import BuildIcon from '@material-ui/icons/Build';
import Select from '@material-ui/core/Select/Select';
import MenuItem from '@material-ui/core/MenuItem/MenuItem';
import ListItemIcon from '@material-ui/core/ListItemIcon/ListItemIcon';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';

import ParameterEdit from './ParameterEdit';
import SimulationOverlayMenu from './SimulationOverlayBar';
import parameters from '../content/parameters';

import style from './TweakBar.sass';

class TweakBar extends React.Component
{
    static propTypes = {
        open: PropTypes.bool.isRequired,
        top: PropTypes.bool.isRequired,
        parameters: PropTypes.object.isRequired,
        onChange: PropTypes.func.isRequired,
        onClose: PropTypes.func.isRequired
    };

    constructor(props)
    {
        super(props);
        this.state = {tweakedParameter: Object.keys(parameters)[0]};
    }
    
    render()
    {
        const compact = !isWidthUp('sm', this.props.width);
        return <SimulationOverlayMenu
                open={this.props.open}
                top={this.props.top}
                onClose={this.props.onClose}
                icon={<BuildIcon />}
                heading={
                    <div className={style.parameterSelect}>
                        <Select
                                className={style.select}
                                value={this.state.tweakedParameter}
                                onChange={event => this.setState({tweakedParameter: event.target.value})}
                            >
                            {
                                Object.entries(parameters).map(
                                    ([paramName, parameter]) => <MenuItem key={paramName} value={paramName}>
                                        {parameter.icon ? <ListItemIcon>{parameter.icon}</ListItemIcon> : null}
                                        {parameter.title}
                                    </MenuItem>
                                )
                            }
                        </Select>
                    </div>
                }
            >
            {compact ? null : <div className={style.divider} />}
            <ParameterEdit
                style={{opacity: .5}}
                parameter={parameters[this.state.tweakedParameter]}
                value={this.props.parameters[this.state.tweakedParameter]}
                onChange={value => this.props.onChange(this.state.tweakedParameter, value)}
            />
        </SimulationOverlayMenu>;
    }
}

export default withWidth()(TweakBar);