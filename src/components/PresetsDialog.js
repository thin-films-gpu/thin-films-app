import Dialog from '@material-ui/core/Dialog/Dialog';
import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import Button from '@material-ui/core/Button/Button';
import GridList from '@material-ui/core/GridList/GridList';
import GridListTile from '@material-ui/core/GridListTile/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar/GridListTileBar';
import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';
import PropTypes from 'prop-types';

import presets from '../content/presets';

import style from './PresetsDialog.sass';

function PresetsDialog(props)
{
    return <Dialog open={props.open} fullWidth={true} classes={{paper: style.dialog}}>
        <DialogTitle>Select a scene</DialogTitle>
        <GridList className={style.presetSelector} cols={isWidthUp('sm', props.width) ? 3 : 2}>
            {
                Object.entries(presets).map(
                    ([presetId, preset]) => (
                        <GridListTile
                                key={presetId}
                                className={style.item}
                                onClick={() => props.onPresetSelected(preset)}
                                onMouseEnter={() => props.onPresetPreview(preset.preview.background)}
                            >
                            <img src={preset.preview.thumbnail} />
                            <GridListTileBar title={preset.title} />
                        </GridListTile>
                    )
                )
            }
        </GridList>
        <div className={style.sceneSetupSection}>
            Or
            <Button className={style.sceneSetupButton} color='primary' variant='text' onClick={props.onContinueToSetup}>
                Set up a custom scene
                <KeyboardArrowRightIcon className={style.icon} />
            </Button>
        </div>
    </Dialog>;
}

PresetsDialog.propTypes = {
    open: PropTypes.bool.isRequired,
    onPresetSelected: PropTypes.func.isRequired,
    onContinueToSetup: PropTypes.func.isRequired,
    onPresetPreview: PropTypes.func.isRequired
};

export default withWidth()(PresetsDialog);