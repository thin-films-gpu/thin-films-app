import {image} from '../util';
import Dialog from '@material-ui/core/Dialog/Dialog';
import React from 'react';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import Button from '@material-ui/core/Button/Button';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';

import style from './SplashDialog.sass';

export default function SplashDialog(props)
{
    return <Dialog open={props.open} fullWidth={true} classes={{paper: style.splash}}>
        <DialogTitle>
            <div className={style.logo}>
                <img className={style.icon} src={image('icon.png')} />
                <div className={style.title}>
                    <div className={style.realTime}>Real-Time</div>
                    <div className={style.thinFilms}>Viscous Thin Films</div>
                </div>
            </div>
        </DialogTitle>
        <div className={style.description}>
            A novel technique allows simulating flow of viscous thin films of liquid on flat surfaces in <em>real time</em>.
        </div>
        <div className={style.attribution}>
            <div className={style.paper}>
                <div className={style.description}>Technical Paper</div>
                <InsertDriveFileIcon />
                <div className={style.title}>
                    <div className={style.name}>Real-time Viscous Thin Films</div>
                    <div className={style.year}>SIGGRAPH ASIA, 2018</div>
                </div>
            </div>
            <div className={style.divider}></div>
            <div className={style.authors}>
                <div className={style.author}>
                    <div className={style.name}>Orestis Vantzos</div>
                    <div className={style.affiliation}>Technion, Math Dept.</div>
                </div>
                <div className={style.author}>
                    <div className={style.name}>Saar Raz</div>
                    <div className={style.affiliation}>Technion, CS Dept.</div>
                </div>
                <div className={style.author}>
                    <div className={style.name}>Miri Ben-Chen</div>
                    <div className={style.affiliation}>Technion, CS Dept.</div>
                </div>
            </div>
        </div>
        <Button color='primary' variant='raised' onClick={props.onContinue}>
            Start demo
        </Button>
    </Dialog>;
};