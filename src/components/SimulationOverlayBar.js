import React from 'react';
import MuiThemeProvider from '@material-ui/core/es/styles/MuiThemeProvider';
import createMuiTheme from '@material-ui/core/es/styles/createMuiTheme';
import IconButton from '@material-ui/core/IconButton/IconButton';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpandLessIcon from '@material-ui/icons/ExpandLess';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';

import PropTypes from 'prop-types';
import classNames from 'classnames';

import style from './SimulationOverlayBar.sass';

function SimulationOverlayBar(props)
{
    const compact = !isWidthUp('sm', props.width);
    const closeButton =
        <IconButton className={style.closeButton} onClick={event => props.onClose(event)}>
            {props.top ? <ExpandLessIcon /> : <ExpandMoreIcon />}
        </IconButton>;
    const closeButtonBeforeContent = compact && props.top;
    return <MuiThemeProvider theme={createMuiTheme({palette: {type: 'dark'}})}>
        <div className={classNames(style.overlayBar, {[style.open]: props.open, [style.top]: props.top})}>
            {closeButtonBeforeContent ? closeButton : null}
            <div className={style.heading}>
                {props.icon ? <div className={style.icon}>{props.icon}</div> : null}
                {props.heading}
            </div>
            <div className={style.content}>
                {props.children}
            </div>
            {closeButtonBeforeContent ? null : closeButton}
        </div>
    </MuiThemeProvider>;
}

SimulationOverlayBar.propTypes = {
    open: PropTypes.bool.isRequired,
    top: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    icon: PropTypes.node,
    heading: PropTypes.node,
    width: PropTypes.string.isRequired
};

export default withWidth()(SimulationOverlayBar);