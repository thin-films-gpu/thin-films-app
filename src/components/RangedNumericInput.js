import React from 'react';
import TextField from '@material-ui/core/TextField/TextField';
import PropTypes from 'prop-types';
import Slider from '@material-ui/lab/Slider/Slider';

import isNumber from 'is-number';

import style from './RangedNumericInput.sass';

export default class RangedNumericInput extends React.Component
{
    static propTypes = {
        min: PropTypes.number.isRequired,
        max: PropTypes.number.isRequired,
        disabled: PropTypes.bool,
        closestAcceptable: PropTypes.func,
        step: PropTypes.number,
        value: PropTypes.number,
        onChange: PropTypes.func.isRequired,
        strictUpperBound: PropTypes.number,
        strictLowerBound: PropTypes.number
    };

    constructor(props)
    {
        super(props);
        this.state = {
            error: false,
            value: props.value,
        };
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if (!this.state.error && this.props.value !== this.state.value) {
            this.setState({value: this.props.value});
        }
    }

    proposeValue(value)
    {
        if (!isNumber(value)
            || this.props.strictUpperBound !== undefined && value > this.props.strictUpperBound
            || this.props.strictLowerBound !== undefined && value < this.props.strictLowerBound
            || this.props.closestAcceptable && this.props.closestAcceptable(value) != value) {
            this.props.onChange(null);
            this.setState({error: true, value});
            return;
        }
        this.props.onChange(value);
        this.setState({error: false, value});
    }

    render()
    {
        return <div className={style.main}>
            <div className={style.sliderContainer}>
                <Slider
                    className={style.slider}
                    disabled={this.props.disabled}
                    min={this.props.min}
                    max={this.props.max}
                    value={this.props.value}
                    onChange={
                        (event, value) => this.proposeValue(
                            this.props.closestAcceptable
                                ? this.props.closestAcceptable(value)
                                : value.toFixed(
                                    -Math.floor(Math.min(Math.log10(this.props.max - this.props.min) - 1, -1))
                                )
                        )
                    }
                />
                <div className={style.labels}>
                    <div className={style.tickedLabel}>
                        <div className={style.tick} />
                        <div className={style.label}>{this.props.min}</div>
                    </div>
                    <div className={style.tickedLabel}>
                        <div className={style.tick} />
                        <div className={style.label}>{this.props.max}</div>
                    </div>
                </div>
            </div>
            <TextField
                disabled={this.props.disabled}
                className={style.textField}
                type='text'
                error={this.state.error}
                value={this.state.value}
                onChange={event => this.proposeValue(event.target.value)}
            />
        </div>;
    }
};