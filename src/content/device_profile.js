
function hasAccelerometer()
{
    return window.DeviceOrientationEvent && 'ontouchstart' in window;
}

function isStrongGPU()
{
    const gl = document.createElement('canvas').getContext('webgl2');
    if (gl) {
        return /(NVIDIA)|(ATI)|(INTEL)/i.test(
            gl.getParameter(gl.getExtension('WEBGL_debug_renderer_info').UNMASKED_RENDERER_WEBGL)
        );
    }
    return false;
}

export default {accelerometer: hasAccelerometer(), strong: isStrongGPU()};