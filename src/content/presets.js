import {image} from '../util';
import ThinFilmsSimulation from '../components/ThinFilmsSimulation';
import backgrounds from "./backgrounds";

export default {
    wineOnBricks: {
        title: 'Wine on Bricks',
        preview: {
            thumbnail: image('presets/wine_on_bricks.png'),
            background: backgrounds.bricks.diffuse
        },
        parameters: {
            background: 'bricks',
            fluid: 'wine',
            initialCondition: 'doodle520',
            shader: ThinFilmsSimulation.shaders.kRefractionAndCaustics
        }
    },
    waterOnWindow: {
        title: 'Water on Window Pane',
        preview: {
            thumbnail: image('presets/water_on_window.png'),
            background: backgrounds.window.diffuse
        },
        parameters: {
            background: 'window',
            fluid: 'water',
            initialCondition: 'doodle520',
            shader: ThinFilmsSimulation.shaders.kRefractionAndCaustics
        }
    },
    honeyOnHoneycomb: {
        title: 'Honey on Honeycomb',
        preview: {
            thumbnail: image('presets/honey_on_honeycomb.png'),
            background: backgrounds.honeycomb.diffuse
        },
        parameters: {
            background: 'honeycomb',
            fluid: 'honey',
            initialCondition: 'doodle520',
            shader: ThinFilmsSimulation.shaders.kRefractionAndCaustics
        }
    },
    gradient: {
        title: 'Clean Demo',
        preview: {
            thumbnail: image('presets/clean_demo.png'),
            background: image('presets/clean_background.png')
        },
        parameters: {
            background: 'window',
            fluid: 'water',
            initialCondition: 'gaussians',
            shader: ThinFilmsSimulation.shaders.kGradient
        }
    },
    wineOnBoard: {
        title: 'Wine on Board',
        preview: {
            thumbnail: image('presets/wine_on_board.png'),
            background: backgrounds.board2.diffuse
        },
        parameters: {
            background: 'board2',
            fluid: 'wine2',
            initialCondition: 'doodle520',
            shader: ThinFilmsSimulation.shaders.kRefractionAndCaustics
        }
    },
    greenWaterOnDeck: {
        title: 'Green Water on Deck',
        preview: {
            thumbnail: image('presets/green_water_on_deck.png'),
            background: backgrounds.deck.diffuse
        },
        parameters: {
            background: 'deck',
            fluid: 'greenWater',
            initialCondition: 'siggraph',
            shader: ThinFilmsSimulation.shaders.kRefractionAndCaustics
        }
    }
};