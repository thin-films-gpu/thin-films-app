import React from 'react';
import FormatPaintIcon from '@material-ui/icons/FormatPaint';
import ThreeSixtyIcon from '@material-ui/icons/ThreeSixty';
import StraightenIcon from '@material-ui/icons/Straighten';
import FitnessCenterIcon from '@material-ui/icons/FitnessCenter';
import GridOnIcon from '@material-ui/icons/GridOn';
import BlurCircularIcon from '@material-ui/icons/BlurCircular';
import ScreenRotationIcon from '@material-ui/icons/ScreenRotation';
import BlurOnIcon from '@material-ui/icons/BlurOn';
import LoopIcon from '@material-ui/icons/Loop';
import OpacityIcon from '@material-ui/icons/Opacity';
import LineWeightIcon from '@material-ui/icons/LineWeight';
import WavesIcon from '@material-ui/icons/Waves';
import TimelapseIcon from '@material-ui/icons/Timelapse';
import TimerIcon from '@material-ui/icons/Timer';
import WallpaperIcon from '@material-ui/icons/Wallpaper';
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleFilledOutlined';
import PhotoSizeSelectLargeIcon from '@material-ui/icons/PhotoSizeSelectLarge';

import {range} from 'range';

import backgrounds from './backgrounds';
import fluids from './fluids';
import initialConditions from './initial_conditions';

import ThinFilmsSimulation from "../components/ThinFilmsSimulation";

import deviceProfile from './device_profile';

export function getParameterDefault(parameter)
{
    if (parameter.default !== null && typeof parameter.default === typeof {}) {
        for (const [criterion, value] of Object.entries(parameter.default)) {
            if (criterion !== 'default' && deviceProfile[criterion]) {
                return value;
            }
        }
        return parameter.default.default;
    }
    return parameter.default;
}

export default {
    periodic: {
        kind: 'bool',
        default: false,
        title: 'Periodic boundary conditions',
        icon: <LoopIcon />
    },
    diffusion: {
        kind: 'number',
        default: 2,
        title: 'Diffusion (η)',
        icon: <BlurOnIcon />,
        range: {
            valid: {from: 0},
            reasonable: {from: 0, to: 20}
        }
    },
    accelerometerGravity: {
        kind: 'bool',
        default: {accelerometer: true, default: false},
        title: 'Accelerometer-controlled gravity',
        icon: <ScreenRotationIcon />
    },
    gravity: {
        kind: 'number',
        default: 30,
        title: 'Gravity (G)',
        icon: <FitnessCenterIcon />,
        range: {
            valid: {from: 0},
            reasonable: {from: 0, to: 100}
        }
    },
    fluidViscosity: {
        kind: 'bool',
        default: true,
        title: 'Use fluid viscosity',
        icon: <OpacityIcon />
    },
    viscosity: {
        kind: 'number',
        default: 4,
        title: 'Viscosity (ε)',
        icon: <LineWeightIcon />,
        range: {
            valid: {from: 0},
            reasonable: {from: 4, to: 15}
        }
    },
    mobility: {
        kind: 'select',
        default: 0,
        title: 'Mobility (M)',
        icon: <WavesIcon />,
        options: [
            {value: 0, title: '2u₁²u₂²/3(u₁ + u₂)'},
            {value: 1, title: '(u₁⁻³ + u₂⁻³)⁻¹'}
        ]
    },
    simulationResolution: {
        kind: 'number',
        default: {strong: 512, default: 256},
        title: 'Simulation resolution',
        icon: <GridOnIcon />,
        range: {
            valid: {from: 2},
            reasonable: {from: 128, to: 1024},
            closestAcceptable:
                value => range(5, 12).map(pow => 2 ** pow).sort((a, b) => Math.abs(a - value) - Math.abs(b - value))[0]
        }
    },
    dynamicIterationCount: {
        kind: 'bool',
        default: {strong: true, default: false},
        title: 'Dynamic iteration count',
        icon: <BlurCircularIcon />
    },
    iterations: {
        kind: 'number',
        default: {strong: 10, default: 3},
        title: 'Iterations',
        icon: <BlurOnIcon />,
        range: {
            valid: {from: 1},
            reasonable: {from: 3, to: 100},
            closestAcceptable: value => Math.round(value)
        }
    },
    dynamicTimeStep: {
        kind: 'bool',
        default: true,
        title: 'Dynamic time step',
        icon: <TimelapseIcon />
    },
    logTimeStep: {
        kind: 'number',
        default: -1,
        title: 'Time step (τ, in powers of 10)',
        icon: <TimerIcon />,
        range: {
            valid: {},
            reasonable: {from: -5, to: -1},
        }
    },
    shader: {
        kind: 'select',
        default: ThinFilmsSimulation.shaders.kRefractionAndCaustics,
        title: 'Shader',
        icon: <FormatPaintIcon />,
        options: [
            {value: ThinFilmsSimulation.shaders.kHeightmap, title: 'Height Map'},
            {value: ThinFilmsSimulation.shaders.kGradient, title: 'Gradient'},
            {value: ThinFilmsSimulation.shaders.kNormal, title: 'Normal Map'},
            {value: ThinFilmsSimulation.shaders.kRefraction, title: 'Refraction'},
            {value: ThinFilmsSimulation.shaders.kRefractionAndCaustics, title: 'Refraction & Caustics'}
        ]
    },
    renderResolution: {
        kind: 'select',
        default: {
            strong: ThinFilmsSimulation.renderResolution.kFull,
            default: ThinFilmsSimulation.renderResolution.kStretch
        },
        title: 'Rendering resolution',
        icon: <PhotoSizeSelectLargeIcon />,
        options: [
            {value: ThinFilmsSimulation.renderResolution.kNormal, title: 'Same as simulation'},
            {value: ThinFilmsSimulation.renderResolution.kStretch, title: 'Same as simulation, stretch to screen size'},
            {value: ThinFilmsSimulation.renderResolution.kFull, title: 'Screen resolution (might affect performance)'}
        ]
    },
    lightDirection: {
        kind: 'number',
        default: 0,
        title: 'Light direction',
        icon: <ThreeSixtyIcon />,
        range: {
            valid: {from: 0, to: 360},
            reasonable: {from: 0, to: 360}
        }
    },
    lightDistance: {
        kind: 'number',
        default: 0,
        title: 'Light distance',
        icon: <StraightenIcon />,
        range: {
            valid: {from: 0},
            reasonable: {from: 0, to: 1}
        }
    },
    smoothingPasses: {
        kind: 'number',
        default: 1,
        title: 'Smoothing passes',
        icon: <BlurOnIcon />,
        range: {
            valid: {from: 0},
            reasonable: {from: 0, to: 5},
            closestAcceptable: value => Math.round(value)
        }
    },
    background: {
        kind: 'select',
        default: null,
        title: 'Background',
        icon: <WallpaperIcon />,
        options: Object.entries(backgrounds).map(([bgId, bg]) => ({value: bgId, title: bg.title}))
    },
    fluid: {
        kind: 'select',
        default: null,
        title: 'Fluid',
        icon: <WavesIcon />,
        options: Object.entries(fluids).map(([fluidId, fluid]) => ({value: fluidId, title: fluid.title}))
    },
    initialCondition: {
        kind: 'select',
        default: null,
        title: 'Initial condition',
        icon: <PlayCircleOutlineIcon />,
        options: Object.entries(initialConditions).map(([condId, cond]) => ({value: condId, title: cond.title}))
    }
};