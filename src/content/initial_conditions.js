import { image } from '../util'

export default {
    empty2: {
        title: 'Empty',
        path: image('empty2.png')
    },
    emptiest: {
        title: 'Emptier',
        path: image('emptiest.png')
    },
    siggraph: {
        title: 'SIGGRAPH Asia 2018',
        path: image('siggraph.png')
    },
    /*
    main_title: {
        title: 'Main Title',
        path: image('main_title.png')
    },*/
    hexagons: {
        title: 'Hexagons',
        path: image('hexagons.png')
    },
    doodle520: {
        title: 'Doodle',
        path: image('doodle520.png')
    },
    kiss: {
        title: 'Kissing Droplets',
        path: image('kiss.png')
    },
    front: {
        title: 'Waterfront',
        path: image('front.png')
    },
    isolated: {
        title: 'Pinball',
        path: image('isolated.png')
    },
    gaussians: {
        title: 'Gaussians',
        path: image('gaussians.png')
    },
    noise520: {
        title: 'Noise',
        path: image('noise520.png')
    },
    tiffanys520: {
        title: 'Tiffany',
        path: image('tiffanys520.png')
    }
};
