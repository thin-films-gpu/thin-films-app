const kWaterRefractiveIndex = 1.33;

export default {
    blood: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [.6, 0., 0.],
        clarity: {clearDepth: 0., opaqueDepth: .8},
        viscosity: 10,
        title: 'Blood'
    },
    wine: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [.4, 0., .05],
        clarity: {clearDepth: 0., opaqueDepth: 1.},
        viscosity: 5.,
        title: 'Wine'
    },
    wine2: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [.2, 0., .1],
        clarity: {clearDepth: 0., opaqueDepth: 6.},
        viscosity: 5.,
        title: 'Wine (alternative)'
    },
    honey: {
        refractiveIndex: 1.504,
        color: [.3, .15, 0.],
        clarity: {clearDepth: 0., opaqueDepth: 5.},
        viscosity: 10,
        title: 'Honey'
    },
    milk: {
        refractiveIndex: 1.3440,
        color: [1., 1., 1.],
        clarity: {clearDepth: 0., opaqueDepth: .8},
        viscosity: 10,
        title: 'Milk'
    },
    /*
    goo: {
        refractiveIndex: 1.3440,
        color: [0., 1., 0.],
        clarity: {clearDepth: 0., opaqueDepth: .7},
        viscosity: 20,
        title: 'Goo'
    },*/
    water: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [.5, .5, .5],
        clarity: {clearDepth: 0., opaqueDepth: 1000.},
        viscosity: 5.,
        title: 'Water'
    },
    greenWater: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [.5, .7, .6],
        clarity: {clearDepth: 0., opaqueDepth: 1000.},
        viscosity: 5.,
        title: 'Green Water'
    },
    /*test: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [1., 1., 1.],
        clarity: {clearDepth: 0., opaqueDepth: 10.},
        viscosity: 10,
        title: 'Test'
    },*/
    juice: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [1., .6, 0.],
        clarity: {clearDepth: 0.1,opaqueDepth:  10.},
        viscosity: 5.,
        title: 'Juice'
    },
    /*
    title: {
        refractiveIndex: kWaterRefractiveIndex,
        color: [.0, .212, 1.],
        clarity: {.clearDepth: 05, 1opaqueDepth: .},
        viscosity: 5.,
        title: 'Title'
    }*/
};