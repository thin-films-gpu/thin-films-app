import { image } from '../util'

export default {
    honeycomb: {
        title: 'Honeycomb',
        diffuse: image('honeycomb.jpg')
    },
    window: {
        title: 'Window Pane',
        diffuse: image('window.jpg')
    },
    concrete: {
        title: 'Concrete',
        diffuse: image('concrete.jpg'),
        bump: image('concrete.jpg'),
        bumpDepth: 1.
    },
    deck: {
        title: 'Deck',
        diffuse: image('deck.jpg')
    },
    /*
    title: {
    title: 'Thin Films Title',
        diffuse: image('main_title.png'),
        bump: image('main_title.bump.png'),
        bumpDepth: 100.,

    },*/
    board: {
        title: 'Board',
        diffuse: image('board.jpg')
    },
    board2: {
        title: 'Board 2',
        diffuse: image('board2.png')
    },
    flatBricks: {
        title: 'Bricks',
        diffuse: image('bricks.diffuse.jpg')
    },
    bricks: {
        title: 'Bricks (shallow bumps)',
        diffuse: image('bricks.diffuse.jpg'),
        bump: image('bricks.bump.jpg'),
        bumpDepth: 2.
    },
    tallBricks: {
        title: 'Bricks (deep bumps)',
        diffuse: image('bricks.diffuse.jpg'),
        bump: image('bricks.bump.jpg'),
        bumpDepth: 4.
    }
};