function static_asset(path)
{
    return 'static/' + path;
}

function image(name)
{
    return static_asset('img/' + name);
}

export { static_asset, image };