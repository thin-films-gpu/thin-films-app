import React from 'react';
import ReactDOM from 'react-dom';
import Tooltip from '@material-ui/core/Tooltip/Tooltip';
import Button from '@material-ui/core/Button/Button';
import CircularProgress from '@material-ui/core/CircularProgress/CircularProgress';
import SpeedDial from '@material-ui/lab/SpeedDial/SpeedDial';
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon/SpeedDialIcon';
import SpeedDialAction from '@material-ui/lab/SpeedDialAction/SpeedDialAction';
import ScatterPlotIcon from '@material-ui/icons/ScatterPlot';
import ReplayIcon from '@material-ui/icons/Replay';
import BuildIcon from '@material-ui/icons/Build';
import BrushIcon from '@material-ui/icons/Brush';
import CollectionsIcon from '@material-ui/icons/Collections';
import SettingsIcon from '@material-ui/icons/Settings';
import OpacityIcon from '@material-ui/icons/Opacity';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import CameraAltIcon from '@material-ui/icons/Camera';
import withWidth, {isWidthUp} from '@material-ui/core/withWidth';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import classNames from 'classnames';
import equals from 'fast-deep-equal';

import backgrounds from './content/backgrounds';
import fluids from './content/fluids';
import initialConditions from './content/initial_conditions';
import parameters, {getParameterDefault} from './content/parameters';
import deviceProfile from './content/device_profile';
import variables from './variables.json';
import SplashDialog from './components/SplashDialog';
import SimulationSetupDialog from './components/SimulationSetupDialog';
import ThinFilmsSimulation from './components/ThinFilmsSimulation';
import TweakBar from './components/TweakBar';
import {image} from './util';

import style from './index.sass';
import SimulationOverlayBar from './components/SimulationOverlayBar';
import RangedNumericInput from './components/RangedNumericInput';
import PresetsDialog from "./components/PresetsDialog";

const theme = createMuiTheme({
    palette: {
        primary: {main: variables.colors.primary},
        secondary: {main: variables.colors.secondary}
    }
});

class Index extends React.Component
{
    static UIState = {
        kSplash: 0,
        kPresets: 1,
        kSimulation: 2,
        kSimulationSetup: 3
    };

    constructor(props)
    {
        super(props);
        this.state = {
            loaded: false,
            uiState: Index.UIState.kSplash,
            previewImage: null,
            frameRate: null,
            isDewetting: false,
            parameters: {...Index.getDefaultParameters(), ...Index.getParametersFromQueryString()},
            running: true,
            speedDialOpen: false,
            tweakBarOpen: false,
            brushBarOpen: false,
            brushRadius: .5,
            sceneResetCounter: 0,
            sceneSnapCounter: 0
        };
        this.simulation = React.createRef();
        this.state.previewImage =
            this.state.parameters.background
                ? backgrounds[this.state.parameters.background].diffuse
                : backgrounds.board2.diffuse;
    }

    static getDefaultParameters()
    {
        return Object.keys(parameters)
            .reduce((obj, paramName) => ({...obj, [paramName]: getParameterDefault(parameters[paramName])}), {});
    }

    static getParametersFromQueryString()
    {
        const urlParams = new URLSearchParams(window.location.search);
        return Object.keys(parameters)
            .map(paramName => [paramName, urlParams.has(paramName) ? JSON.parse(urlParams.get(paramName)) : null])
            .filter(([paramName, value]) => value !== null)
            .reduce((obj, [paramName, value]) => ({...obj, [paramName]: value}), {});
    }

    static writeParametersToQueryString(currentParameters)
    {
        const urlParams = new URLSearchParams(window.location.search);
        for (const [paramName, value] of Object.entries(currentParameters)) {
            if (!equals(getParameterDefault(parameters[paramName]), value)) {
                urlParams.set(paramName, JSON.stringify(value));
            } else {
                urlParams.delete(paramName);
            }
        }
        const newUrl =
            `${window.location.protocol}//${window.location.host}${window.location.pathname}?${urlParams.toString()}`;
        window.history.pushState({path: newUrl}, '', newUrl);
    }

    canStartSimulation()
    {
        return Object.keys(parameters)
            .every(paramName => paramName in this.state.parameters && this.state.parameters[paramName] !== null);
    }

    componentDidUpdate(prevProps, prevState, snapshot)
    {
        if (!equals(prevState.parameters, this.state.parameters)) {
            Index.writeParametersToQueryString(this.state.parameters);
            if (!this.state.parameters.accelerometerGravity) {
                this.simulation.current.angle = 0;
                this.simulation.current.steepness = 1;
            }
        }
    }

    componentDidMount()
    {
        window.addEventListener(
            'deviceorientation',
            event =>
            {
                if (event.gamma === null) {
                    return;
                }
                const vertical = Math.max(-1, Math.min(1, event.gamma / 45.));
                const horizontal = Math.max(-1, Math.min(1, event.beta / 45.));
                this.simulation.current.angle = Math.atan2(vertical, horizontal);
                this.simulation.current.steepness = Math.sqrt(vertical * vertical + horizontal * horizontal);
                event.preventDefault();
            },
            true
        );
        window.onload = event => this.setState({loaded: true});
    }

    static lightLocation(angle, distance)
    {
        const angleRadians = angle * (2 * Math.PI / 360);
        return {x: Math.cos(angleRadians) * distance, y: Math.sin(angleRadians) * distance, z: 1}
    }

    snap(snapURL)
    {
        const binStr = atob(snapURL.split(',')[1]);
        const len = binStr.length;
        const arr = new Uint8Array(len);

        for (let i = 0; i < len; i++) {
            arr[i] = binStr.charCodeAt(i);
        }

        const blob = new Blob([arr]);
        const link = document.createElement('a');
        link.download = `snap_t=${this.simulation.current.totalTime}.png`;
        link.innerHTML = 'download';
        link.href = URL.createObjectURL(blob);
        link.onclick = () => {
            requestAnimationFrame(() => {
                URL.revokeObjectURL(link.href);
                link.removeAttribute('href');
            });
        };
        link.click();
    }

    onKeyPress(event)
    {
        switch (event.key) {
            case 'x':
                this.setState({running: !this.state.running});
                break;
            case 'o':
                this.setState({isDewetting: !this.state.isDewetting});
                break;
            case 't':
                this.setState({tweakBarOpen: !this.state.tweakBarOpen, brushBarOpen: false});
                break;
            case 'b':
                this.setState({brushBarOpen: !this.state.brushBarOpen, tweakBarOpen: false});
                break;
            case 'r':
                this.setState({sceneResetCounter: this.state.sceneResetCounter + 1});
                break;
            case 's':
                this.setState({sceneSnapCounter: this.state.sceneSnapCounter + 1});
                break;
            default:
                return;
        }
        event.preventDefault();
        event.stopPropagation();
    }

    render()
    {
        let isTouch;
        if (typeof document !== 'undefined') {
          isTouch = 'ontouchstart' in document.documentElement;
        }

        return <MuiThemeProvider theme={theme}>
            <meta name='theme-color' content={variables.colors.primary} />
            <meta name='msapplication-navbutton-color' content={variables.colors.primary} />
            <meta name='apple-mobile-web-app-status-bar-style' content={variables.colors.primary} />

            <SplashDialog
                open={this.state.uiState === Index.UIState.kSplash}
                onContinue={() => this.setState({uiState: Index.UIState.kPresets})}
            />
            <PresetsDialog
                open={this.state.uiState === Index.UIState.kPresets}
                onPresetPreview={preview => this.setState({previewImage: preview})}
                onContinueToSetup={() => this.setState({uiState: Index.UIState.kSimulationSetup})}
                onPresetSelected={
                    preset => this.setState({
                        uiState: Index.UIState.kSimulation,
                        parameters: {...this.state.parameters, ...preset.parameters}
                    })
                }
            />
            <SimulationSetupDialog
                open={this.state.uiState === Index.UIState.kSimulationSetup}
                onBackgroundSelected={
                    backgroundId => this.setState({shownBackground: backgrounds[backgroundId].diffuse})
                }
                onContinue={parameters => this.setState({parameters, uiState: Index.UIState.kSimulation})}
                initialParameters={this.state.parameters}
            />
            <div className={classNames(style.loadingScreen, {[style.loaded]: this.state.loaded})}>
                <img className={style.icon} src={image('icon.png')}/>
                <CircularProgress className={style.indicator}/>
            </div>
            <div
                    className={
                        classNames(
                            style.app,
                            {
                                [style.unfocused]: this.state.uiState !== Index.UIState.kSimulation,
                                [style.paused]: !this.state.running
                            }
                        )
                    }
                    style={{backgroundImage: `url(${this.state.previewImage})`}}
                    tabIndex={0}
                    onKeyPress={event => this.onKeyPress(event)}
                >
                {
                    !this.canStartSimulation()
                        ? null
                        : <ThinFilmsSimulation
                            className={style.simulation}
                            ref={this.simulation}
                            running={this.state.uiState === Index.UIState.kSimulation}
                            background={backgrounds[this.state.parameters.background]}
                            fluid={fluids[this.state.parameters.fluid]}
                            diffusion={this.state.parameters.diffusion}
                            viscosity={this.state.parameters.fluidViscosity ? null : this.state.parameters.viscosity}
                            gravity={{
                                strength: this.state.parameters.gravity
                            }}
                            initialConditionPath={initialConditions[this.state.parameters.initialCondition].path}
                            isDewetting={this.state.isDewetting}
                            brushRadius={this.state.brushRadius}
                            iterations={this.state.parameters.iterations}
                            mobility={this.state.parameters.mobility}
                            screenWidth={window.innerWidth}
                            screenHeight={window.innerHeight}
                            shader={this.state.parameters.shader}
                            simulationResolution={this.state.parameters.simulationResolution}
                            dynamicTimeStep={this.state.parameters.dynamicTimeStep}
                            logTimeStep={this.state.parameters.logTimeStep}
                            paused={!this.state.running}
                            periodic={this.state.parameters.periodic}
                            renderResolution={this.state.parameters.renderResolution}
                            reportFrameRate={
                                deviceProfile.strong ? frameRate => this.setState({frameRate}) : frameRate => null
                            }
                            lightLocation={Index.lightLocation(
                                this.state.parameters.lightDirection,
                                this.state.parameters.lightDistance
                            )}
                            smoothingPasses={this.state.parameters.smoothingPasses}
                            sceneResetCounter={this.state.sceneResetCounter}
                            sceneSnapCounter={this.state.sceneSnapCounter}
                            onSnap={snapURL => this.snap(snapURL)}
                        />
                }
                {
                    deviceProfile.strong
                        ? <div className={style.frameRateCounter}>
                            <div className={style.caption}>FPS</div>
                            <div
                                className={style.number}>{this.state.frameRate ? this.state.frameRate.toFixed(0) : null}
                            </div>
                        </div>
                        : null
                }
                <TweakBar
                    open={this.state.tweakBarOpen}
                    top={!isWidthUp('sm', this.props.width)}
                    parameters={this.state.parameters}
                    onChange={
                        (paramName, value) =>
                            this.setState({parameters: {...this.state.parameters, [paramName]: value}})
                    }
                    onClose={() => this.setState({tweakBarOpen: false})}
                />
                <SimulationOverlayBar
                    open={this.state.brushBarOpen}
                    onClose={() => this.setState({brushBarOpen: false})}
                    icon={<BrushIcon />}
                    heading={
                        <RangedNumericInput
                            min={0.1}
                            max={1}
                            strictLowerBound={.001}
                            value={this.state.brushRadius}
                            onChange={value => this.setState({brushRadius: value})}
                        />
                    }
                    top={!isWidthUp('sm', this.props.width)}
                />
                <div className={style.mainControls}>
                    <SpeedDial
                            className={style.speedDial}
                            ariaLabel={''}
                            icon={
                                <SpeedDialIcon
                                    icon={this.state.isDewetting ? <ScatterPlotIcon /> : <OpacityIcon />}
                                />
                            }
                            open={this.state.speedDialOpen}
                            //onBlur={() => this.setState({speedDialOpen: false})}
                            onClick={() => this.setState({speedDialOpen: true})}
                            onClose={() => this.setState({speedDialOpen: false})}
                            onMouseEnter={isTouch ? undefined : () => this.setState({speedDialOpen: true})}
                            onMouseLeave={() => this.setState({speedDialOpen: false})}
                        >
                        {
                            this.state.isDewetting
                                ? <SpeedDialAction
                                    tooltipTitle='Add fluid mode (O)'
                                    icon={<OpacityIcon />}
                                    onClick={() => this.setState({speedDialOpen: false, isDewetting: false})}
                                />
                                : <SpeedDialAction
                                    tooltipTitle='Obstacle mode (O)'
                                    icon={<ScatterPlotIcon />}
                                    onClick={() => this.setState({speedDialOpen: false, isDewetting: true})}
                                />
                        }
                        <SpeedDialAction
                            tooltipTitle='Tweak parameters (T)'
                            icon={<BuildIcon />}
                            onClick={
                                () => this.setState({speedDialOpen: false, tweakBarOpen: true, brushBarOpen: false})
                            }
                        />
                        <SpeedDialAction
                            tooltipTitle='Brush settings (B)'
                            icon={<BrushIcon />}
                            onClick={
                                () => this.setState({speedDialOpen: false, brushBarOpen: true, tweakBarOpen: false})
                            }
                            onTouch={
                                () => this.setState({speedDialOpen: false, brushBarOpen: true, tweakBarOpen: false})
                            }
                        />
                        <SpeedDialAction
                            tooltipTitle='Scene setup'
                            icon={<SettingsIcon />}
                            onClick={
                                () => this.setState({speedDialOpen: false, uiState: Index.UIState.kSimulationSetup})
                            }
                        />
                        <SpeedDialAction
                            tooltipTitle='Reset scene (R)'
                            icon={<ReplayIcon />}
                            onClick={
                                () => this.setState({
                                    speedDialOpen: false,
                                    sceneResetCounter: this.state.sceneResetCounter + 1
                                })
                            }
                        />
                        <SpeedDialAction
                            tooltipTitle='Save snapshot (S)'
                            icon={<CameraAltIcon />}
                            onClick={
                                () => this.setState({
                                    speedDialOpen: false,
                                    sceneSnapCounter:
                                        this.state.sceneSnapCounter + 1
                                })
                            }
                        />
                        <SpeedDialAction
                            tooltipTitle='Go to presets'
                            icon={<CollectionsIcon />}
                            onClick={() => this.setState({speedDialOpen: false, uiState: Index.UIState.kPresets})}
                        />
                    </SpeedDial>
                    <Tooltip title={`${this.state.running ? 'Pause' : 'Resume'} (X)`}>
                        <Button
                                className={style.playPauseButton}
                                color={this.state.running ? 'primary' : 'secondary'}
                                variant={'fab'}
                                onClick={() => this.setState({running: !this.state.running})}
                            >
                            {this.state.running ? <PauseIcon /> : <PlayArrowIcon />}
                        </Button>
                    </Tooltip>
                </div>
            </div>
        </MuiThemeProvider>;
    }
}

const IndexWithWidth = withWidth()(Index);

ReactDOM.render(<IndexWithWidth />, document.getElementById('index'));