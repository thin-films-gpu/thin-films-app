#version 300 es
in vec2 a_position;
in vec2 a_texCoord;
uniform bool u_flip;
out vec2 p;

void main()
{
    gl_Position = vec4(u_flip ? vec2(a_position.x, -a_position.y) : a_position, 0, 1);
    p = a_texCoord;
}