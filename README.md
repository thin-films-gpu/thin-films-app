# Real-Time Viscous Thin Films

This is the code for "Real-Time Viscous Thin Films" as published in SIGGRAPH Asia 2018.
Actual thin-film code is under /src/shaders/thin_films.frag and in the `draw()` method in /src/components/ThinFilmsSimulation.js .
